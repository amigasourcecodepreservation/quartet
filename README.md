
Quartet - is a classic music program for Amiga and Atari, originally published by Microdeal. The Quartet versions on the two platforms were actually two totally different music programs, done by different authors, and only shared name and box design.

## Development

Quartet for the Atari ST was done by two University students, Rob Povey and Kewin Cowtan. Dan Lennard, also a University student at the time wrote the Amiga version. Both versions were written in assembler. Dan went on to write a few more titles for Microdeal.


<img src="/img/microdeal/quartet/quartet_box_front_atari.jpg" width="400" />

## Open Source?

Quartet for both Amiga and Atari has been available at various sites for many years, but in 2018 it was clearly given permission by all involved to be released under an open source license, the MIT License, by permission from the authors and from the former founder and Microdeal publisher John Symes. This is part of an ongoing effort to release the Microdeal/Michtron-titles for legal preservation.

Sadly, any source code for both versions has been lost in time, so all we can offer at the moment is the image files for usage in an emulator. 


## Help!!

- Are you a former Microdeal/Michtron contact or worker and happen have an old backup of the source code for any of the platforms? Please help us and send it to our email. It is totally fine to send it anonymously.
- Do you want to resource the programs to restore the source code. Go ahead, we encourage you, just don't forget to notify us so we can put it up on our gitlab.

## Trivia

## Many thanks to 

John Symes for being positive about the project and giving permission, Rob Povey, Kevin Cowtan and Dan Lennard for being permissive and supporting of preserving their old work under liberal usage and license.

